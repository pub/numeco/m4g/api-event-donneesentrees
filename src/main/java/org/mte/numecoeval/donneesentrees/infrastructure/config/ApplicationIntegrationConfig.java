package org.mte.numecoeval.donneesentrees.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.common.exception.NumEcoEvalRuntimeException;
import org.mte.numecoeval.common.integration.interceptors.NumEcoEvalHeadersForLogChannelInterceptor;
import org.mte.numecoeval.common.utils.PreparedStatementUtils;
import org.mte.numecoeval.common.utils.ResultSetUtils;
import org.mte.numecoeval.topic.data.ApplicationDTO;
import org.mte.numecoeval.topic.data.MessageCalculIndicateurApplicationDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactEquipementVirtuelDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Splitter;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.config.GlobalChannelInterceptor;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.jdbc.JdbcOutboundGateway;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.GenericMessage;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * Configuration de Spring Integration pour la partie Application.
 * Couvre 2 contrats AsyncAPI
 */
@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.app"
)
public class ApplicationIntegrationConfig {

    @Value("${numecoeval.topic.entree.application}")
    String topicEntreeApplication;

    @Value("${numecoeval.topic.calcul.application}")
    String topicCalculApplication;

    @Bean
    public NewTopic topicEntreeApplication() {
        return new NewTopic(topicEntreeApplication, 1, (short) 1);
    }

    @Bean
    public NewTopic topicCalculApplication() {
        return new NewTopic(topicCalculApplication, 1, (short) 1);
    }

    // Channels
    @Bean
    public MessageChannel applicationStartChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel applicationTransformMessageIndicateurChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel applicationJdbcSplitter() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel applicationTransformJdbcOutToCalculChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel applicationOutputChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    @GlobalChannelInterceptor(patterns = "*")
    public ChannelInterceptor applicationChannelInterceptor() {
        return new NumEcoEvalHeadersForLogChannelInterceptor();
    }

    // Entrées
    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessageIndicateurImpactEquipementVirtuelDTO> indicateurImpactEquipementVirtuelKafkaAdapter(KafkaMessageListenerContainer<String, MessageIndicateurImpactEquipementVirtuelDTO> containerImpactEquipementVirtuel) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(containerImpactEquipementVirtuel, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("applicationStartChannel");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessageIndicateurImpactEquipementVirtuelDTO> impactEquipementVirtuelKafkaContainer(ConsumerFactory<String, MessageIndicateurImpactEquipementVirtuelDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicEntreeApplication);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    @Transformer(
            inputChannel = "applicationStartChannel",
            outputChannel = "applicationTransformMessageIndicateurChannel"
    )
    public Message<MessageIndicateurImpactEquipementVirtuelDTO> transformerMessageIndicateurImpactEquipementVirtuelDTO(Message<MessageIndicateurImpactEquipementVirtuelDTO> message) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, message.getPayload().getEquipementVirtuel().getNomLot());
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, Objects.toString(message.getPayload().getEquipementVirtuel().getDateLot(), ""));
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, message.getPayload().getEquipementVirtuel().getNomOrganisation());
        headers.put("dateLotSql", PreparedStatementUtils.getDateFromLocalDate(message.getPayload().getEquipementVirtuel().getDateLot()));
        headers.put("originalPayload", message.getPayload());
        log.info("Recherche d'applications correspondantes : nom Equipement Virtuel: {}, nom equipement physique : {}, nom entité : {}, id : {} ",
                message.getPayload().getEquipementVirtuel().getNomEquipementVirtuel(),
                message.getPayload().getEquipementVirtuel().getNomEquipementPhysique(),
                message.getPayload().getEquipementVirtuel().getNomEntite(),
                message.getPayload().getEquipementVirtuel().getId()
        );
        return new GenericMessage<>(message.getPayload(), headers);
    }

    // Recherche des équipements virtuels
    @Bean
    @ServiceActivator(
            inputChannel = "applicationTransformMessageIndicateurChannel",
            outputChannel = "applicationJdbcSplitter"
    )
    public MessageHandler jdbcApplicationHandler(DataSource dataSource) {
        JdbcOutboundGateway jdbcMessageHandler = new JdbcOutboundGateway(dataSource,
                """
                UPDATE en_equipement_virtuel
                SET statut_traitement = 'TRAITE', date_update = now()
                WHERE id = :payload.equipementVirtuel.id
                """,
                """
                SELECT *
                FROM en_application
                WHERE
                    nullif(nom_lot, :payload.equipementVirtuel.nomLot) is null
                    AND nullif(nom_equipement_virtuel, :payload.equipementVirtuel.nomEquipementVirtuel) is null
                    AND nullif(nom_equipement_physique, :payload.equipementVirtuel.nomEquipementPhysique) is null
                    AND nullif(nom_organisation, :payload.equipementVirtuel.nomOrganisation) is null
                    AND nullif(date_lot, :headers[dateLotSql]) is null
                """

                );
        jdbcMessageHandler.setRowMapper((rs, rowNum) ->
                mapResultSetToApplicationDTO(rs)
        );
        jdbcMessageHandler.setMaxRows(1000);
        return jdbcMessageHandler;
    }

    private static ApplicationDTO mapResultSetToApplicationDTO(ResultSet rs) throws SQLException {
        log.info("Mapping de l'application {} - {}: nom Equipement Virtuel: {}, nom equipement physique : {}, nom entité : {}, id : {} ",
                rs.getString("nom_application"),
                rs.getString("type_environnement"),
                rs.getString("nom_equipement_virtuel"),
                rs.getString("nom_equipement_physique"),
                rs.getString("nom_entite"),
                rs.getLong("id")
                );
        return ApplicationDTO.builder()
                .id(rs.getLong("id"))
                .nomLot(rs.getString("nom_lot"))
                .dateLot(ResultSetUtils.getLocalDate(rs, "date_lot"))
                .nomOrganisation(rs.getString("nom_organisation"))
                .nomEntite(rs.getString("nom_entite"))
                .nomEquipementPhysique(rs.getString("nom_equipement_physique"))
                .nomEquipementVirtuel(rs.getString("nom_equipement_virtuel"))
                .nomApplication(rs.getString("nom_application"))
                .typeEnvironnement(rs.getString("type_environnement"))
                .domaine(rs.getString("domaine"))
                .sousDomaine(rs.getString("sous_domaine"))
                .nomSourceDonnee(rs.getString("nom_source_donnee"))
                .build();
    }

    // Splitter de la liste JDBC vers en message unitaire
    @Splitter(
            inputChannel = "applicationJdbcSplitter",
            outputChannel = "applicationTransformJdbcOutToCalculChannel"
    )
    public List<ApplicationDTO> splitterListApplication(Message<List<ApplicationDTO>> messageList) {
        return new ArrayList<>(messageList.getPayload());
    }

    // Transformation de la sortie JDBC
    @Transformer(
            inputChannel = "applicationTransformJdbcOutToCalculChannel",
            outputChannel = "applicationOutputChannel"
    )
    public Message<MessageCalculIndicateurApplicationDTO> transformerJdbcOutputToMessageCalculIndicateurApplicationDTO(Message<ApplicationDTO> message) {
        MessageIndicateurImpactEquipementVirtuelDTO originalPayload = (MessageIndicateurImpactEquipementVirtuelDTO) message.getHeaders().get("originalPayload");

        if(originalPayload == null) {
            log.error("Impossible de traiter le message : Header avec Payload Original null : {}", message);
            throw new NumEcoEvalRuntimeException("Impossible de traiter le message : Header avec Payload Original null : Date Lot: " + message.getPayload().getDateLot() + " : Nom Organisation : " + message.getPayload().getNomOrganisation() + " : Nom VM : "+ message.getPayload().getNomEquipementVirtuel());
        }

        log.info("Mapping pour calcul de l'instance d'application {} - {}: nom équipement virtuel : {}, nom equipement physique : {}, nom entité : {}, id : {}, nombre indicateur: {}",
                message.getPayload().getNomApplication(),
                message.getPayload().getTypeEnvironnement(),
                message.getPayload().getNomEquipementVirtuel(),
                message.getPayload().getNomEquipementPhysique(),
                message.getPayload().getNomEntite(),
                message.getPayload().getId(),
                CollectionUtils.size(originalPayload.getImpactsEquipement())
                );
        var headers = new HashMap<String, Object>();
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, originalPayload.getEquipementVirtuel().getNomLot());
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, Objects.toString(originalPayload.getEquipementVirtuel().getDateLot(),""));
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, originalPayload.getEquipementVirtuel().getNomOrganisation());

        return new GenericMessage<>(
                MessageCalculIndicateurApplicationDTO.builder()
                        .application(message.getPayload())
                        .nbApplications(originalPayload.getEquipementVirtuel().getNbApplications())
                        .indicateursEquipementsVirtuels(originalPayload.getImpactsEquipement())
                        .build(),
                headers
        );
    }

    // Sortie du MessageCalculEquipementVirtuelDTO vers Kafka
    @Bean
    @ServiceActivator(inputChannel = "applicationOutputChannel")
    public MessageHandler messageCalculIndicateurApplicationDTOHandler(KafkaTemplate<String, MessageCalculIndicateurApplicationDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageCalculIndicateurApplicationDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicCalculApplication));
        return handler;
    }
}
