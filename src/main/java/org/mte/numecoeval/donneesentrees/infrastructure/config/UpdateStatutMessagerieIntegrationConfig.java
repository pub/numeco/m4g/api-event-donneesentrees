package org.mte.numecoeval.donneesentrees.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactMessagerieDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.jdbc.JdbcMessageHandler;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import javax.sql.DataSource;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.mes"
)
public class UpdateStatutMessagerieIntegrationConfig {

    @Value("${numecoeval.topic.entree.update.messagerie}")
    String topicUpdateEntreesMessagerie;


    // 6.5 - Entrée Mise à jour des données d'entrées
    @Bean
    public MessageChannel updateStatutMessagerieChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessageIndicateurImpactMessagerieDTO> updateStatutMessagerieKafkaAdapter(KafkaMessageListenerContainer<String, MessageIndicateurImpactMessagerieDTO> containerMessageIndicateurImpactApplication) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(containerMessageIndicateurImpactApplication, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("updateStatutMessagerieChannel");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessageIndicateurImpactMessagerieDTO> updateStatutMessagerieKafkaContainer(ConsumerFactory<String, MessageIndicateurImpactMessagerieDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicUpdateEntreesMessagerie);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // Mise à jour des entrées en bases

    @Bean
    @ServiceActivator(
            inputChannel = "updateStatutMessagerieChannel"
    )
    public MessageHandler jdbcUpdateStatutMessagerie(DataSource dataSource) {
        return new JdbcMessageHandler(dataSource,
                """
                UPDATE en_messagerie
                SET statut_traitement = 'TRAITE', date_update = now()
                WHERE id = :payload.messagerie.id
                """
        );
    }
}
