package org.mte.numecoeval.donneesentrees.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.common.exception.NumEcoEvalRuntimeException;
import org.mte.numecoeval.common.integration.interceptors.NumEcoEvalHeadersForLogChannelInterceptor;
import org.mte.numecoeval.common.utils.PreparedStatementUtils;
import org.mte.numecoeval.common.utils.ResultSetUtils;
import org.mte.numecoeval.topic.data.EquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageCalculEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactEquipementPhysiqueDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Splitter;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.config.GlobalChannelInterceptor;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.jdbc.JdbcOutboundGateway;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.GenericMessage;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.eqv"
)
public class EquipementVirtuelIntegrationConfig {

    @Value("${numecoeval.topic.entree.equipementVirtuel}")
    String topicEntreesEquipementVirtuel;

    @Value("${numecoeval.topic.calcul.equipementVirtuel}")
    String topicCalculEquipementVirtuel;

    @Bean
    public NewTopic topicEntreesEquipementVirtuel() {
        return new NewTopic(topicEntreesEquipementVirtuel, 1, (short) 1);
    }

    @Bean
    public NewTopic topicCalculEquipementVirtuel() {
        return new NewTopic(topicCalculEquipementVirtuel, 1, (short) 1);
    }

    // Channels
    @Bean
    public MessageChannel equipementVirtuelStartChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel equipementVirtuelTransformMessageIndicateurChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel equipementVirtuelJdbcSplitter() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel equipementVirtuelTransformJdbcOutToCalculChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel equipementVirtuelOutputChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    @GlobalChannelInterceptor(patterns = "*")
    public ChannelInterceptor equipementVirtuelChannelInterceptor() {
        return new NumEcoEvalHeadersForLogChannelInterceptor();
    }

    // Entrées
    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessageIndicateurImpactEquipementPhysiqueDTO> indicateurImpactEquipementPhysiqueKafkaAdapter(KafkaMessageListenerContainer<String, MessageIndicateurImpactEquipementPhysiqueDTO> containerImpactEquipementPhysique) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(containerImpactEquipementPhysique, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("equipementVirtuelStartChannel");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessageIndicateurImpactEquipementPhysiqueDTO> impactEquipementPhysiqueKafkaContainer(ConsumerFactory<String, MessageIndicateurImpactEquipementPhysiqueDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicEntreesEquipementVirtuel);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    @Transformer(
            inputChannel = "equipementVirtuelStartChannel",
            outputChannel = "equipementVirtuelTransformMessageIndicateurChannel"
    )
    public Message<MessageIndicateurImpactEquipementPhysiqueDTO> transformerMessageIndicateurImpactEquipementPhysiqueDTO(Message<MessageIndicateurImpactEquipementPhysiqueDTO> message) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, message.getPayload().getEquipementPhysique().getNomLot());
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, Objects.toString(message.getPayload().getEquipementPhysique().getDateLot(), ""));
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, message.getPayload().getEquipementPhysique().getNomOrganisation());
        headers.put("dateLotSql", PreparedStatementUtils.getDateFromLocalDate(message.getPayload().getEquipementPhysique().getDateLot()));
        headers.put("originalPayload", message.getPayload());
        log.info("Recherche d'équipements virtuels correspondant : nom equipement physique : {}, nom entité : {}, id : {} ",
                message.getPayload().getEquipementPhysique().getNomEquipementPhysique(),
                message.getPayload().getEquipementPhysique().getNomEntite(),
                message.getPayload().getEquipementPhysique().getId()
        );
        return new GenericMessage<>(message.getPayload(), headers);
    }

    // Recherche des équipements virtuels
    @Bean
    @ServiceActivator(
            inputChannel = "equipementVirtuelTransformMessageIndicateurChannel",
            outputChannel = "equipementVirtuelJdbcSplitter"
    )
    public MessageHandler jdbcEquipementVirtuelHandler(DataSource dataSource) {
        JdbcOutboundGateway jdbcMessageHandler = new JdbcOutboundGateway(dataSource,
                """
                UPDATE en_equipement_physique
                SET statut_traitement = 'TRAITE', date_update = now()
                WHERE id = :payload.equipementPhysique.id
                """,
                """
                SELECT *
                FROM en_equipement_virtuel
                WHERE
                    nullif(nom_lot, :payload.equipementPhysique.nomLot) is null
                    AND nullif(nom_equipement_physique, :payload.equipementPhysique.nomEquipementPhysique) is null
                    AND nullif(nom_organisation, :payload.equipementPhysique.nomOrganisation) is null
                    AND nullif(date_lot, :headers[dateLotSql]) is null
                """

                );
        jdbcMessageHandler.setRowMapper((rs, rowNum) ->
                mapResultSetToEquipementVirtuelDTO(rs)
        );
        jdbcMessageHandler.setMaxRows(1000);
        return jdbcMessageHandler;
    }

    private static EquipementVirtuelDTO mapResultSetToEquipementVirtuelDTO(ResultSet rs) throws SQLException {
        log.info("Mapping de l'équipement virtuel {}: nom equipement physique : {}, nom entité : {}, id : {} ",
                rs.getString("nom_equipement_virtuel"),
                rs.getString("nom_equipement_physique"),
                rs.getString("nom_entite"),
                rs.getLong("id")
                );
        return EquipementVirtuelDTO.builder()
                .id(rs.getLong("id"))
                .nomLot(rs.getString("nom_lot"))
                .dateLot(ResultSetUtils.getLocalDate(rs, "date_lot"))
                .nomOrganisation(rs.getString("nom_organisation"))
                .nomEntite(rs.getString("nom_entite"))
                .nomEquipementPhysique(rs.getString("nom_equipement_physique"))
                .nomEquipementVirtuel(rs.getString("nom_equipement_virtuel"))
                .cluster(rs.getString("cluster"))
                .vCPU(ResultSetUtils.getInteger(rs, "vcpu"))
                .consoElecAnnuelle(ResultSetUtils.getDouble(rs, "conso_elec_annuelle"))
                .typeEqv(rs.getString("type_eqv"))
                .capaciteStockage(ResultSetUtils.getDouble(rs, "capacite_stockage"))
                .cleRepartition(ResultSetUtils.getDouble(rs, "cle_repartition"))
                .nbApplications(ResultSetUtils.getInteger(rs, "nb_applications"))
                .nomSourceDonnee(rs.getString("nom_source_donnee"))
                .build();
    }

    // Splitter de la liste JDBC vers en message unitaire
    @Splitter(
            inputChannel = "equipementVirtuelJdbcSplitter",
            outputChannel = "equipementVirtuelTransformJdbcOutToCalculChannel"
    )
    public List<EquipementVirtuelDTO> splitterListEquipementVirtuel(Message<List<EquipementVirtuelDTO>> messageList) {
        return new ArrayList<>(messageList.getPayload());
    }

    // Transformation de la sortie JDBC
    @Transformer(
            inputChannel = "equipementVirtuelTransformJdbcOutToCalculChannel",
            outputChannel = "equipementVirtuelOutputChannel"
    )
    public Message<MessageCalculEquipementVirtuelDTO> transformerJdbcOutputToMessageCalculEquipementVirtuelDTO(Message<EquipementVirtuelDTO> message) {
        MessageIndicateurImpactEquipementPhysiqueDTO originalPayload = (MessageIndicateurImpactEquipementPhysiqueDTO) message.getHeaders().get("originalPayload");

        if(originalPayload == null) {
            log.error("Impossible de traiter le message : Header avec Payload Original null : {}", message);
            throw new NumEcoEvalRuntimeException("Impossible de traiter le message : Header avec Payload Original null : Nom Lot: " + message.getPayload().getNomLot() + " Date Lot: " + message.getPayload().getDateLot() + " : Nom Organisation : " + message.getPayload().getNomOrganisation() + " : Nom VM : "+ message.getPayload().getNomEquipementVirtuel());
        }

        log.info("Mapping pour calcul de l'équipement virtuel {}: nom equipement physique : {}, nom entité : {}, id : {}, nombre indicateur: {}",
                message.getPayload().getNomEquipementVirtuel(),
                message.getPayload().getNomEquipementPhysique(),
                message.getPayload().getNomEntite(),
                message.getPayload().getId(),
                CollectionUtils.size(originalPayload.getImpactsEquipement())
                );
        var headers = new HashMap<String, Object>();
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_LOT, originalPayload.getEquipementPhysique().getNomLot());
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_DATELOT, Objects.toString(originalPayload.getEquipementPhysique().getDateLot(),""));
        headers.put(NumEcoEvalHeadersForLogChannelInterceptor.HEADER_NOM_ORGANISATION, originalPayload.getEquipementPhysique().getNomOrganisation());

        return new GenericMessage<>(
                MessageCalculEquipementVirtuelDTO.builder()
                        .equipementVirtuel(message.getPayload())
                        .nbEquipementsVirtuels(originalPayload.getEquipementPhysique().getNbEquipementsVirtuels())
                        .nbTotalVCPU(originalPayload.getEquipementPhysique().getNbTotalVCPU())
                        .stockageTotalVirtuel(originalPayload.getEquipementPhysique().getStockageTotalVirtuel())
                        .impactsEquipement(originalPayload.getImpactsEquipement())
                        .build(),
                headers
        );
    }

    // Sortie du MessageCalculEquipementVirtuelDTO vers Kafka
    @Bean
    @ServiceActivator(inputChannel = "equipementVirtuelOutputChannel")
    public MessageHandler messageCalculEquipementVirtuelDTOHandler(KafkaTemplate<String, MessageCalculEquipementVirtuelDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageCalculEquipementVirtuelDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicCalculEquipementVirtuel));
        return handler;
    }


}
