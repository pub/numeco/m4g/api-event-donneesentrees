package org.mte.numecoeval.donneesentrees.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.MessageIndicateurApplicationDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.jdbc.JdbcMessageHandler;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import javax.sql.DataSource;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.app"
)
public class UpdateStatutApplicationIntegrationConfig {

    @Value("${numecoeval.topic.entree.update.application}")
    String topicUpdateEntreesApplication;

    // 4.6 - Entrée Mise à jour des données d'entrées
    @Bean
    public MessageChannel updateStatutApplicationChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessageIndicateurApplicationDTO> updateStatutApplicationKafkaAdapter(KafkaMessageListenerContainer<String, MessageIndicateurApplicationDTO> containerMessageIndicateurImpactApplication) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(containerMessageIndicateurImpactApplication, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("updateStatutApplicationChannel");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessageIndicateurApplicationDTO> updateStatutApplicationKafkaContainer(ConsumerFactory<String, MessageIndicateurApplicationDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicUpdateEntreesApplication);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // Mise à jour des entrées en bases

    @Bean
    @ServiceActivator(
            inputChannel = "updateStatutApplicationChannel"
    )
    public MessageHandler jdbcUpdateStatutApplication(DataSource dataSource) {
        return new JdbcMessageHandler(dataSource,
                """
                UPDATE en_application
                SET statut_traitement = 'TRAITE', date_update = now()
                WHERE id = :payload.application.id
                """
        );
    }
}
