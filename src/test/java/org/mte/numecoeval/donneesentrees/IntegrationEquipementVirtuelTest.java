package org.mte.numecoeval.donneesentrees;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.donneesentrees.test.jdbc.ScriptUtils;
import org.mte.numecoeval.donneesentrees.test.kafka.KafkaConsumerEquipementVirtuel;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactEquipementPhysiqueDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = { "test" })
@AutoConfigureEmbeddedDatabase
@EmbeddedKafka(
        bootstrapServersProperty = "spring.kafka.bootstrap-servers",
        partitions = 1
)
class IntegrationEquipementVirtuelTest {

    @Value("${numecoeval.topic.entree.equipementVirtuel}")
    String topicToSend;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    KafkaConsumerEquipementVirtuel kafkaConsumer;

    @Autowired
    KafkaTemplate<String, MessageIndicateurImpactEquipementPhysiqueDTO> kafkaTemplate;

    @BeforeEach
    void setup() {
        jdbcTemplate.batchUpdate("DELETE FROM en_data_center");
        jdbcTemplate.batchUpdate("DELETE FROM en_equipement_physique");
        jdbcTemplate.batchUpdate("DELETE FROM en_equipement_virtuel");
    }

    @Test
    void whenDataAvailable_shouldUpdateStatusAndSendMessage() {
        // Given
        ScriptUtils.loadScripts(jdbcTemplate.getDataSource(), Arrays.asList("sql/equipment_physique.sql", "/sql/equipement_virtuel.sql"));

        var nombreEquipementVirtuel = 10;

        var message = Instancio.of(MessageIndicateurImpactEquipementPhysiqueDTO.class).create();
        message.getEquipementPhysique().setNomLot("ENTITE|2022-01-01");
        message.getEquipementPhysique().setNomOrganisation("ENTITE");
        message.getEquipementPhysique().setNomEntite("ENTITE");
        message.getEquipementPhysique().setNomCourtDatacenter("TestDataCenter");
        message.getEquipementPhysique().setNomEquipementPhysique("2023-03-07-ServeurCalcul-1010");
        message.getEquipementPhysique().setDateLot(LocalDate.of(2022, 1, 1));
        message.getEquipementPhysique().setNbTotalVCPU(79);
        message.getEquipementPhysique().setStockageTotalVirtuel(3560.0);
        message.getEquipementPhysique().setId(377003L);

        var rows = jdbcTemplate.query(
                "SELECT * from en_equipement_physique WHERE id = 377003",
                (rs, rowNum) -> {
                    var row = new HashMap<String, Object>();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        row.put(rs.getMetaData().getColumnLabel(i).toUpperCase(), rs.getObject(i));
                    }
                    return row;
                }
        );

        // When
        var resultSend = kafkaTemplate.send(topicToSend, message).join();

        // Then
        assertEquals(1, rows.size());
        assertNotNull(resultSend);
        await().atMost(10, TimeUnit.SECONDS)
                .until(() -> {
                    var queryResultCountEntreesIngerees = jdbcTemplate.query(
                            "SELECT count(*) as nbr from en_equipement_physique WHERE statut_traitement = 'TRAITE'",
                            (rs, rowNum) -> rs.getInt("nbr")
                    );

                    return !queryResultCountEntreesIngerees.isEmpty() && Objects.equals(1, queryResultCountEntreesIngerees.get(0));
                });
        await().atMost(5, TimeUnit.SECONDS)
                .until(() -> nombreEquipementVirtuel == kafkaConsumer.getPayloads().size() );

    }

}
