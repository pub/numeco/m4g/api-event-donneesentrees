package org.mte.numecoeval.donneesentrees;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.donneesentrees.test.jdbc.ScriptUtils;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactMessagerieDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = { "test" })
@AutoConfigureEmbeddedDatabase
@EmbeddedKafka(
        bootstrapServersProperty = "spring.kafka.bootstrap-servers",
        partitions = 1
)
class IntegrationUpdateStatutMessagerieTest {

    @Value("${numecoeval.topic.entree.update.messagerie}")
    String topicToSend;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    KafkaTemplate<String, MessageIndicateurImpactMessagerieDTO> kafkaTemplate;

    @BeforeEach
    void setup() {
        jdbcTemplate.batchUpdate("DELETE FROM en_messagerie");
    }

    @Test
    void whenDataAvailable_shouldUpdateStatusAndSendMessage() {
        ScriptUtils.loadScript(jdbcTemplate.getDataSource(), "sql/messagerie.sql");

        // Given
        var message = Instancio.of(MessageIndicateurImpactMessagerieDTO.class).create();
        message.getMessagerie().setId(11002L);
        message.getMessagerie().setDateLot(LocalDate.of(2022,1,1));
        message.getMessagerie().setNomOrganisation("ENTITE");
        message.getMessagerie().setNomEntite("ENTITE");
        message.getMessagerie().setMoisAnnee(201901);
        message.getMessagerie().setNombreMailEmis(123456);
        message.getMessagerie().setNombreMailEmisXDestinataires(513);
        message.getMessagerie().setVolumeTotalMailEmis(84351385);

        // When
        var resultSend = kafkaTemplate.send(topicToSend, message).join();


        // Then
        assertNotNull(resultSend);
        await().atMost(5, TimeUnit.SECONDS)
                .until(() -> {
                    var queryResultCountEntreesIngerees = jdbcTemplate.query(
                            "SELECT count(*) as nbr from en_messagerie WHERE statut_traitement = 'TRAITE'",
                            (rs, rowNum) -> rs.getInt("nbr")
                    );

                    return !queryResultCountEntreesIngerees.isEmpty() && Objects.equals(1, queryResultCountEntreesIngerees.get(0));
                });

    }

}
