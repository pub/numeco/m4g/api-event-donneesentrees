package org.mte.numecoeval.donneesentrees.test.kafka;

import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.MessageCalculEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageCalculIndicateurApplicationDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, EquipementPhysiqueDTO> kafkaListenerContainerFactoryEquipementPhysique(ConsumerFactory<String, EquipementPhysiqueDTO> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, EquipementPhysiqueDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public KafkaConsumer<EquipementPhysiqueDTO> kafkaConsumerEquipementPhysique(){
        return new KafkaConsumerEquipementPhysique();
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, MessageCalculEquipementVirtuelDTO> kafkaListenerContainerFactoryEquipementVirtuel(ConsumerFactory<String, MessageCalculEquipementVirtuelDTO> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, MessageCalculEquipementVirtuelDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public KafkaConsumer<MessageCalculEquipementVirtuelDTO> kafkaConsumerEquipementVirtuel(){
        return new KafkaConsumerEquipementVirtuel();
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, MessageCalculIndicateurApplicationDTO> kafkaListenerContainerFactoryApplication(ConsumerFactory<String, MessageCalculIndicateurApplicationDTO> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, MessageCalculIndicateurApplicationDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public KafkaConsumer<MessageCalculIndicateurApplicationDTO> kafkaConsumerApplication(){
        return new KafkaConsumerApplication();
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, MessagerieDTO> kafkaListenerContainerFactoryMessagerie(ConsumerFactory<String, MessagerieDTO> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, MessagerieDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public KafkaConsumer<MessagerieDTO> kafkaConsumerMessagerie(){
        return new KafkaConsumerMessagerie();
    }

}
