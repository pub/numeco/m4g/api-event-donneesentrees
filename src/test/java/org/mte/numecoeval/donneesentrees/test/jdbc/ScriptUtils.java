package org.mte.numecoeval.donneesentrees.test.jdbc;

import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ScriptUtils {

    public static void loadScript(DataSource dataSource, String scriptSql) {
        loadScripts(dataSource, Collections.singletonList(scriptSql));
    }

    public static void loadScripts(DataSource dataSource, List<String> scriptsSqls) {
        assertNotNull(dataSource);

        var populator = new ResourceDatabasePopulator();
        for(var scriptSQL : scriptsSqls) {
            populator.addScripts(
                    new ClassPathResource(scriptSQL)
            );
        }
        populator.execute(dataSource);
    }
}
