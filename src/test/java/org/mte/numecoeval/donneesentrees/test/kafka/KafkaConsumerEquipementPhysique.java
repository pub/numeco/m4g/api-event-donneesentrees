package org.mte.numecoeval.donneesentrees.test.kafka;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
public class KafkaConsumerEquipementPhysique extends KafkaConsumer<EquipementPhysiqueDTO> {

    @Override
    @KafkaListener(topics = "${numecoeval.topic.entree.equipementPhysique}")
    public void consumeMessage(EquipementPhysiqueDTO message) {
        log.info("#############  Playload = {}", message);
        payloads.add(message);
        latch.countDown();

    }
}
