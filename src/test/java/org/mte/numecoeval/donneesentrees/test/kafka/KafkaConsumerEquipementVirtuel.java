package org.mte.numecoeval.donneesentrees.test.kafka;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.MessageCalculEquipementVirtuelDTO;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
public class KafkaConsumerEquipementVirtuel extends KafkaConsumer<MessageCalculEquipementVirtuelDTO> {

    @Override
    @KafkaListener(topics = "${numecoeval.topic.calcul.equipementVirtuel}")
    public void consumeMessage(MessageCalculEquipementVirtuelDTO message) {
        log.info("#############  Playload = {}", message);
        payloads.add(message);
        latch.countDown();

    }
}
