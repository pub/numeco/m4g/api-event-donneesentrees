package org.mte.numecoeval.donneesentrees.test.kafka;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.MessageCalculIndicateurApplicationDTO;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
public class KafkaConsumerApplication extends KafkaConsumer<MessageCalculIndicateurApplicationDTO> {

    @Override
    @KafkaListener(topics = "${numecoeval.topic.calcul.application}")
    public void consumeMessage(MessageCalculIndicateurApplicationDTO message) {
        log.info("#############  Playload = {}", message);
        payloads.add(message);
        latch.countDown();

    }
}
